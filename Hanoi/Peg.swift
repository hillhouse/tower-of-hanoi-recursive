//
//  Peg.swift
//  Hanoi
//
//  Created by Ross on 24/06/2021.
//

import Foundation

class Peg {
  
  init (number : Int){
    self.number = number
  }
  
  var disks = [Disk]()
  
  var number = 0
 
  func add(disk : Disk){
    disks.append(disk)
  }
  
  func remove() -> Disk? {
    return disks.popLast()
  }
  
  func clear(){
    disks = [Disk]()
  }
  
  
}
