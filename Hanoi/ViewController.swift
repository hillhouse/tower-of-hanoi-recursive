//
//  ViewController.swift
//  Hanoi
//
//  Created by Ross on 10/05/2021.
//

import UIKit



class ViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
  
  
  @IBOutlet weak var disk1View: UIView!
  @IBOutlet weak var disk2View: UIView!
  @IBOutlet weak var disk3View: UIView!
  @IBOutlet weak var disk4View: UIView!
  @IBOutlet weak var disk5View: UIView!
  
  @IBOutlet weak var peg1ContainerView: UIView!
  @IBOutlet weak var peg2ContainerView: UIView!
  @IBOutlet weak var peg3ContainerView: UIView!
  
  @IBOutlet weak var slot11: UIView!
  @IBOutlet weak var slot12: UIView!
  @IBOutlet weak var slot13: UIView!
  @IBOutlet weak var slot14: UIView!
  @IBOutlet weak var slot15: UIView!
  
  @IBOutlet weak var slot21: UIView!
  @IBOutlet weak var slot22: UIView!
  @IBOutlet weak var slot23: UIView!
  @IBOutlet weak var slot24: UIView!
  @IBOutlet weak var slot25: UIView!
  
  @IBOutlet weak var slot31: UIView!
  @IBOutlet weak var slot32: UIView!
  @IBOutlet weak var slot33: UIView!
  @IBOutlet weak var slot34: UIView!
  @IBOutlet weak var slot35: UIView!
  
  // @IBOutlet weak var webView: WKWebView!
  @IBOutlet weak var tableView: UITableView!
  
  @IBAction func goButtonClick(_ sender: Any) {
    if !isRunning{
      
      self.isRunning = true
      
      self.highlightedRow = 0
      
      DispatchQueue.global(qos: .background).async {
        
        Thread.sleep(forTimeInterval: 1)
        self.move(level: self.currentLevel, source: self.peg1, target: self.peg3, auxiliary: self.peg2)
      }
      
      self.isRunning = false
      
    }
  }
  
  
  var peg1 = Peg(number: 1)
  var peg2 = Peg(number: 2)
  var peg3 = Peg(number: 3)
  var pegs : [Peg]!
  
  var isRunning = false
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupPegs()
    
    let startingLevel = self.pegs[0].disks.count
    currentLevel = startingLevel
    
    assignDisksToPegs()
    
    tableView.delegate = self
    tableView.dataSource = self
    tableView.reloadData()
  }
  
  
  var currentLevel = 0{
    didSet{
      DispatchQueue.main.async {
        self.tableView.reloadData()
      }
    }
  }
  
  
  func move( level : Int, source : Peg, target : Peg, auxiliary : Peg){
    
    currentLevel = level
    
    self.highlightedRow = 1
    
    if level > 0{
      
      self.highlightedRow = 2
      
   //   Thread.sleep(forTimeInterval: 0.5)
      self.move(level: level - 1, source: source, target: auxiliary, auxiliary: target)
      
      self.highlightedRow = 3
      
      self.transfer(from: source, to: target )
      
      self.highlightedRow = 4
      
      self.move(level: level - 1, source: auxiliary, target: target, auxiliary: source)
      
      self.highlightedRow = 5
      
    }
    
    self.highlightedRow = 6
  }
  
  
  func transfer( from source : Peg, to target: Peg){
    let disk = source.remove()
    target.add(disk: disk!)
    redraw()
  }
  
  
  func redraw(){
    DispatchQueue.main.async {
      self.assignDisksToPegs()
      self.view.setNeedsDisplay()
      
    }
  }
  
  
  var highlightedRow : Int?{
    didSet{
      
      Thread.sleep(forTimeInterval: 1)
      
      DispatchQueue.main.async {
        let selectIndexPath = IndexPath(row: self.highlightedRow!, section: 0)
        self.tableView.selectRow(at: selectIndexPath, animated: true, scrollPosition: .none)
      }
      
    }
  }
  
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 7
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = UITableViewCell()
    
    let label = cell.textLabel!
    
    label.font =  UIFont(name: "SFMono-Regular", size: 12)
     
    let c1 = UIColor(red: 11/255, green: 79/255, blue: 121/255, alpha: 1)
    let c2 = UIColor(red: 28/255, green: 70/255, blue: 74/255, alpha: 1)
    
    var text = ""
    var rangeCurrentLevel : NSRange?
    
    
    switch indexPath.row{
    case 0 : text = "func move( level : " + currentLevel.description + ",  source : Peg,  target : Peg,  auxiliary : Peg ){"
      rangeCurrentLevel = (text as NSString).range(of: currentLevel.description)
    case 1 : text = "    if level > 0{"
    case 2 : text = "        move( level: level - 1,  source: source,  target: auxiliary,  auxiliary: target )"
    case 3 : text = "        transfer( from: source,  to: target )"
    case 4 : text = "        move( level: level - 1,  source: auxiliary,  target: target,  auxiliary: source )"
    case 5 : text = "    }"
    case 6 : text = "}"
    default:()
    }
    
    let rangesPeg = (text as NSString).ranges(of: "Peg")
    let range0 = (text as NSString).range(of: "0")
    let rangeMinusOne = (text as NSString).range(of: "- 1")
    let rangeIf = (text as NSString).range(of: "if")
    let rangeMove = (text as NSString).range(of: "move")
    let rangeFunc = (text as NSString).range(of: "func")
    let boldItalicsFont = UIFont(name: "Avenir-HeavyOblique", size: 18.0)!
    let mutableAttributedString = NSMutableAttributedString.init(string: text)
    
    mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.purple, range: rangeIf)
    mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: c1, range: rangeMove)
    mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.purple, range: rangeFunc)
    mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue, range: range0)
    mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue, range: rangeMinusOne)
    
    for range in rangesPeg{
      mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: c2, range: range)
    }
    
    if rangeCurrentLevel != nil{
      mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: rangeCurrentLevel!)
      mutableAttributedString.addAttribute(NSAttributedString.Key.font, value: boldItalicsFont, range: rangeCurrentLevel!)
    }
    
    if highlightedRow != nil{
      let selectIndexPath = IndexPath(row: highlightedRow!, section: 0)
      DispatchQueue.main.async {
        self.tableView.selectRow(at: selectIndexPath, animated: true, scrollPosition: .none)
      }
    }
    
    label.attributedText = mutableAttributedString
    return cell
  }
  
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return tableView.frame.height / 8
  }
  
  
  func setupPegs(){
    
    pegs = [Peg]()
    
    peg1.clear()
    peg2.clear()
    peg3.clear()
    
    peg1.add(disk: Disk(size: 5, view: disk5View))
    peg1.add(disk: Disk(size: 4, view: disk4View))
    peg1.add(disk: Disk(size: 3, view: disk3View))
    peg1.add(disk: Disk(size: 2, view: disk2View))
    peg1.add(disk: Disk(size: 1, view: disk1View))
    
    pegs = [peg1, peg2, peg3]
  }
  
  
  func assignDisksToPegs(){
    
    peg1ContainerView.clearSubSubViews()
    peg2ContainerView.clearSubSubViews()
    peg3ContainerView.clearSubSubViews()
    
    for peg in pegs{
      
      switch peg.number {
      case 1:
        for (i, disk) in peg.disks.enumerated(){
          switch i {
          case 0: addSubViewAndCenter(parent: slot15, subview: disk.view)
          case 1: addSubViewAndCenter(parent: slot14, subview: disk.view)
          case 2: addSubViewAndCenter(parent: slot13, subview: disk.view)
          case 3: addSubViewAndCenter(parent: slot12, subview: disk.view)
          case 4: addSubViewAndCenter(parent: slot11, subview: disk.view)
          default: ()
          }
        }
      case 2:
        for (i, disk) in peg.disks.enumerated(){
          switch i {
          case 0: addSubViewAndCenter(parent: slot25, subview: disk.view)
          case 1: addSubViewAndCenter(parent: slot24, subview: disk.view)
          case 2: addSubViewAndCenter(parent: slot23, subview: disk.view)
          case 3: addSubViewAndCenter(parent: slot22, subview: disk.view)
          case 4: addSubViewAndCenter(parent: slot21, subview: disk.view)
          default: ()
          }
        }
      case 3:
        for (i, disk) in peg.disks.enumerated(){
          switch i {
          case 0: addSubViewAndCenter(parent: slot35, subview: disk.view)
          case 1: addSubViewAndCenter(parent: slot34, subview: disk.view)
          case 2: addSubViewAndCenter(parent: slot33, subview: disk.view)
          case 3: addSubViewAndCenter(parent: slot32, subview: disk.view)
          case 4: addSubViewAndCenter(parent: slot31, subview: disk.view)
          default: ()
          }
        }
      default:()
      }
    }
  }
  
  
  func addSubViewAndCenter( parent : UIView, subview: UIView ){
    
    parent.addSubview(subview)
    
    UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
      parent.addSubview(subview)
    }, completion: nil)
    
    
    let centerVertically = NSLayoutConstraint(item: subview, attribute: .centerX, relatedBy: .equal, toItem: parent, attribute: .centerX, multiplier: 1.0, constant: 0.0)
    let centerHorizontally = NSLayoutConstraint(item: subview, attribute: .centerY, relatedBy: .equal, toItem: parent, attribute: .centerY, multiplier: 1.0, constant: 0.0)
    
    let propertioanlWidth = 1000 / CGFloat(subview.tag) 
    
    parent.addConstraint(NSLayoutConstraint(item: parent, attribute: .width, relatedBy: .equal, toItem: subview, attribute: .width, multiplier: propertioanlWidth ,constant: 0))
    
    NSLayoutConstraint.activate([centerVertically, centerHorizontally])
  }
}


extension NSString {
  open func ranges(of searchString: String) -> [NSRange] {
    var ranges = [NSRange]()
    var searchRange = NSRange(location: 0, length: self.length)
    var range: NSRange = self.range(of: searchString)
    while range.location != NSNotFound {
      ranges.append(range)
      searchRange = NSRange(location: NSMaxRange(range), length: self.length - NSMaxRange(range))
      range = self.range(of: searchString, options: [], range: searchRange)
    }
    return ranges
  }
}


extension String {
  func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
    var ranges: [Range<Index>] = []
    while let range = self.range(of: substring, options: options, range: (ranges.last?.upperBound ?? self.startIndex)..<self.endIndex, locale: locale) {
      ranges.append(range)
    }
    return ranges
  }
}


extension UIView{
  func clearSubSubViews()  {
    for view in self.subviews{
      for view1 in view.subviews{
        view1.removeFromSuperview()
      }
    }
  }
}
