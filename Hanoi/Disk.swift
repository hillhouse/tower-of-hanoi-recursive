//
//  Disk.swift
//  Hanoi
//
//  Created by Ross on 24/06/2021.
//

import UIKit

class Disk {
  init (size : Int, view : UIView){
    self.size = size
    self.view = view
  }
  var size = 0
  var view : UIView
  
}
